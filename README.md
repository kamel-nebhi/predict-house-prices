# Predict house prices

In this repository, I am using linear regression to predict the price of housing in UK. 

The dataset used for this experiment can be found at http://bit.ly/1Fq0svx and a description of the date can be found at http://bit.ly/1hh97JI


### Data preparation

To start, we need to prepare our data.

First, we want to split the file into train and test dataset. 

We use this command:
```
awk -F, '{if($3 ~ /^"2015-/) print > "test.csv";else print > "train.csv"}'  pp-complete.csv
```
Then we’ve pre-processed both files to normalize the fields and exclude others (based on the exercise description).

First we normalized the date:
```python
temp = pd.DatetimeIndex(chunk['Date of transfer'])
chunk['Date'] = temp.date
chunk['Year'] = temp.year
chunk['Month'] = temp.month
```
We delete 'date of transfer'

```python
del chunk['Date of transfer']
```
We select the columns to keep:
```python
cols_to_keep = ['Month', 'Year', 'Property type', 'Duration', 'Town/City', 'Price']
```

We normalize the ‘town/city’ column with 0 for any city different from London and 1 for London:
```python
chunk.loc[chunk['Town/City'] != 'LONDON', 'Town/City'] = 0
chunk.loc[chunk['Town/City'] == 'LONDON', 'Town/City'] = 1
```
We replace characteristics for 'Duration' and 'Property type'
```python
chunk['Duration'] = chunk['Duration'].map({'F': 0, 'L': 1}).astype(int)
chunk['Property type'] = chunk['Property type'].map({'D': 1, 'S': 2, 'T': 3, 'F': 4, 'O': 5}).astype(int)
```
We put price to logn to normalize the values
```python
chunk["Price"] = np.log1p(chunk["Price"])
```

We define the type of data
```python
chunk['Town/City'] = chunk['Town/City'].astype(int)
chunk['Year'] = chunk['Year'].astype(int)
chunk['Month'] = chunk['Month'].astype(int)
```

To work with large dataset, we use the chunksize attribute 'read_csv' function such as:
```python
chunksize = 100000
mydfs = []
i=0
for chunk in pd.read_csv("data/train.csv", chunksize=chunksize, parse_dates=[2], infer_datetime_format=True, error_bad_lines=True, names=['TUI', 'Price', 'Date of transfer', 'Postcode', 'Property type', 'Old/New', 'Duration', 'PAON', 'SAON', 'Street', 'Locality', 'Town/City', 'District', 'County', 'PPD Category Type','Record Status']):
	# pre-process data here 

	output = chunk[cols_to_keep]
	mydfs.append(output)

	i += 1
	print("chunk ", i, " append to mydfs")

df = pd.concat(mydfs)
print(df.head())
df.to_csv("data/train-norm.csv", sep=',', encoding='utf-8', index=False)
```

After this pre-processing, we’ve obtain 1001974 entries for the testing file and 20069633 entries for the training file.

### Linear regression model

As we have a training and test set, it will be a supervised approach. This is a regression problem as we have to predict a price (not a classification problem).

We decided to use a linear regression model for this first experiment. To learn a model we use the scikit-learn library in Python

We’ve also used the chunksize parameter to avoid any memory issue.

We use the train test split method:
```python
X_train = df_train[:,1:4]
X_test  = df_test[:,5]
y_train = df_train[:,1:4]
y_test  = df_test[:,5]

X, y = df_train[:,1:4], df_train[:,5]

X_train, X_test, Y_train, Y_test = train_test_split(X, y, train_size=0.8, random_state=1)

lm_model = linear_model.LinearRegression()
lm_model.fit(X_train,Y_train)


Predicted_price= lm_model.predict(X_test)
Result = pd.DataFrame({"Observed":np.expm1(Y_test),"Predicted":np.expm1(Predicted_price)})
print(Result.head(10))


```

We load the 10 first comparison between observation and prediction:
```python
Predicted_price= lm_model.predict(X_test)
Result = pd.DataFrame({"Observed":np.expm1(Y_test),"Predicted":np.expm1(Predicted_price)})
print(Result.head(10))
```

We use the explained variance score and the mean absolute error to measure the performance model:
```python
print(explained_variance_score(y, Predicted_price))
print(metrics.mean_absolute_error(y, Predicted_price))

```
We try to find the correlation between the price and other features:
```python
corr = df_train.select_dtypes(include = ['float64', 'int64']).corr()
print(corr['Price'].sort_values(ascending = False))
```
For this first experiment we have obtained an explained variance score of 34.8 and a mean absolute error score of 47.9.

<img src="img/plot.png" width="40%">



